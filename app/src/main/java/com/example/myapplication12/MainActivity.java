package com.example.myapplication12;

import android.content.Intent;
import android.icu.text.IDNA;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText name;
    EditText passwword;
    TextView textView;
    Button login;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.editText2);
        passwword = (EditText) findViewById(R.id.editText5);
        textView = (TextView) findViewById(R.id.textView);
        login = (Button) findViewById(R.id.button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validate(name.getText().toString(), passwword.getText().toString());
            }
        });
    }

    private void validate(String userName, String userPassword) {

//        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
//        startActivity(intent);
        if (userName.equals("Admin") && (userPassword.equals("1234"))) {
            Intent intent = new Intent(MainActivity.this, SecondActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Wrong password", Toast.LENGTH_LONG).show();
        }
    }
}
