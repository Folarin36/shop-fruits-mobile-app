package com.example.myapplication12;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {
    GridView gridView;

    String[] fruitName = {"Apple", "Orange", "Strawberry", "Kiwi", "Melon", "Banana"};
    int[] fruitImages = {R.drawable.apple, R.drawable.orange, R.drawable.strawberry, R.drawable.kiwi, R.drawable.melon, R.drawable.banana};

    @Override
    protected  void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        gridView = findViewById(R.id.gridview);

        CustomAdapter customAdapter = new CustomAdapter();
        gridView.setAdapter(customAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), GridItemActivity.class);
                intent.putExtra("name", fruitName[i]);
                intent.putExtra("name", fruitImages[i]);
                startActivity(intent);

            }

        });
    }

    private class CustomAdapter extends BaseAdapter {
        @Override
        public int getCount() { return fruitImages.length;}

        @Override
        public Object getItem(int i) {
            return null;
        }

         @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View view1 = getLayoutInflater().inflate(R.layout.row_data,null);
            TextView name = view1.findViewById(R.id.text1);
            ImageView image = view1.findViewById(R.id.apple1);

            name.setText(fruitName [i]);
            image.setImageResource(fruitImages [i]);
            return view1;
        }
    }
}



